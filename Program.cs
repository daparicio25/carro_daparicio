﻿using System;

namespace Daparicio_Carro
{
    class Carro
    {
        public string color { get; set; }
        public int cantidad_puertas { get; set; }
        public int revoluciones_x_min { get; set; }
        public double precio { get; set; }
        public bool radio { get; set; }

        static void Main(string[] args)
        {
            Carro carro = new Carro();

            //PINTURA
            Console.Write("Ingresa el color del Carro (blanco, negro, gris, rojo): ");
            carro.Color(Console.ReadLine());

            //PUERTAS
            int opc = 0;
            do {
            Console.Write("Ingresa la cantidad de puertas del Carro (2 o 4): ");
            if (!int.TryParse(Console.ReadLine(), out opc)) {
                Console.WriteLine("Debes ingresar un numero (2 o 4)");
            }

            if (opc == 2 || opc == 4) 
                carro.Puertas(opc);
            else
                Console.WriteLine("Debes ingresar un numero (2 o 4)");
            } while(opc != 2 && opc != 4);

            //REVOLUCIONES
            opc = 0;
            do {
            Console.Write("Ingresa las revoluciones por minuto (1000, 2000 o 3000): ");
            if (!int.TryParse(Console.ReadLine(), out opc)) {
                Console.WriteLine("Debes ingresar un numero (1000, 2000 o 3000)");
            }

            if (opc == 1000 || opc == 2000 || opc == 3000) 
                carro.RevolucionesXMinuto(opc);
            else
                Console.WriteLine("Debes ingresar un numero (1000, 2000 o 3000)");
            } while(opc != 1000 && opc != 2000 && opc != 3000);

            //RADIO
            opc = 0;
            do {
            Console.Write("Ingresa si debe tener radio (0 -> NO) o (1 -> SI): ");
            if (!int.TryParse(Console.ReadLine(), out opc)) {
                Console.WriteLine("Debes ingresar un numero (0 o 1)");
            }

            if (opc == 0 || opc == 1)
                carro.Radio(opc == 0 ? false : true);
            else
                Console.WriteLine("Debes ingresar un numero (0 o 1)");
            } while(opc != 0 && opc != 1 );

            //CARACTERISTICAS
            carro.ImprimirCaracteristicas();
        }

        public void Color(string color) {
            this.color = color;

            switch (this.color) {
                case "blanco":
                    this.precio += 2500.50;
                    break;
                case "negro":
                    this.precio += 3000;
                    break;
                case "gris":
                    this.precio += 3500.75;
                    break;
                case "rojo":
                    this.precio += 4000;
                    break;
                default:
                    this.precio += 2000;
                    break;
            }
        }
    
        public void Puertas(int cantidad_puertas) {
            this.cantidad_puertas = cantidad_puertas;

            if (this.cantidad_puertas == 2) {
                this.precio += 1000.25;
            } else if (this.cantidad_puertas == 4) {
                this.precio += 2000.63;
            }
        }

        public void RevolucionesXMinuto(int revoluciones_x_min) {
            this.revoluciones_x_min = revoluciones_x_min;

            if (this.revoluciones_x_min == 1000) {
                this.precio += 5000.89;
            } else if (revoluciones_x_min == 2000) {
                this.precio += 10000.25;
            } else if (revoluciones_x_min == 3000) {
                this.precio += 15000.16;
            }
        }

        public void Radio(bool radio) {
            this.radio = radio;

            if (this.radio) {
                this.precio += 2750.25;
            }
        }

        public double Precio() {
            return this.precio;
        }

        public void ImprimirCaracteristicas() {
            Console.WriteLine("--- CARACTERISTICAS ---");
            Console.WriteLine("Color: " + this.color);
            Console.WriteLine("Cantidad de puertas: " + this.cantidad_puertas);
            Console.WriteLine("Revoluciones por minuto: " + this.revoluciones_x_min);
            Console.WriteLine("Radio: " + this.radio);
            Console.WriteLine("TOTAL: $" + this.precio);
        }
    }
}